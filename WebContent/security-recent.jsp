<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" errorPage="ErrorPage.jsp"%>
<%@page import="org.apache.commons.jxpath.servlet.ServletContextHandler"%>
<%@page import="javax.script.ScriptEngine"%>
<%@page import="javax.script.ScriptException"%>
<%@page import="javax.script.*"%>
<%@page import="java.sql.ResultSet"%>
<%@ page import="com.integration.http.Tools_thread"%>
<%@ page import="javax.servlet.ServletConfig"%>
<%@ page import="javax.servlet.ServletContext"%>


<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
 
<style>
#overlay {
	background-color: #000;
	display: none;
	height: 100%;
	opacity: 0.89;
	position: fixed;
	width: 100%;
	z-index:1;
}
.ref-class {
    padding: 0 0 10px 65px;
}
#overlay p {
	color: red;
	font-weight: bold;
}

#ToolText .active span {
    background-image: url("tick.png");
    background-repeat: no-repeat;
    display: inline-block;
    height: 13px;
    left: 20px;
    position: relative;
    top: 5px;
    width: 13px;
    z-index: 1;
}

#ToolText {
    left: 90px;
    position: relative;
    z-index:1;
}
progress {
    width: 350px;
}

#overlay-content-main {
color: red;
	left: 38%;
	position: absolute;
	top: 200px;
	z-index:1;
	font-weight: bold;
}

.parent {
    border: 1px solid red;
    height: 420px;
    left: -320px;
    overflow: hidden;
    position: relative;
    width: 960px;
}
.thankyou{
font-size:19px;
margin-top:20%;
}
.swipe {
    position: relative;
    width: 6000px;
}
.ArrowLeft1 {   
    position: absolute;
    top: 175px;
	left:0px;
}
.ArrowRight1 {
    position: absolute;
    right: 0;
    top: 175px;
}
#Slider1 ul{margin:0;padding:0;}
#Slider1 ul li {
    float: left;
}
</style>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<link rel="icon" href="/images/fav.ico" />

<title>Security Framework - A framework to secure your content
	and protect your website</title>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script type="text/javascript" src="js/src.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"
	type="text/javascript">	
</script>
</head>
<body>
 <%
 response.addHeader("Cache-Control", "no-cache,no-store,private,must-revalidate,max-stale=0,post-check=0,pre-check=0"); 
 response.addHeader("Pragma", "no-cache"); 
 response.addDateHeader ("Expires", 0);
            String a=session.getAttribute("username").toString();
 out.println("<p style=\"position:absolute; right:20px; top:35px;\" ><a href=\"Logoutprocess.jsp\" >Logout</a></p> "); 
 
 
            %>
	<div class="wrapper clearfix">
	<div id='overlay'>
	</div>	
	<div id='overlay-content-main' style='display:none'>
	<div id='overlay-content'>
	</div>
	<div class='parent'>
	<div class='swipe' id='Slider1'>
		<ul>
		     <li><img  src="slide1.jpg" alt="Slide1" class='con_trd_gallery'/></li>
		     <li><img  src="slide2.jpg" alt="Slide2" class='con_trd_gallery'/></li>
		     <li><img src="slide3.jpg" alt="Slide3" class='con_trd_gallery'/> </li> 
		     <li><img src="slide4.jpg" alt="Slide4" class='con_trd_gallery'/></li>
		     <li><img src="slide5.jpg" alt="Slide5" class='con_trd_gallery'/></li>
		</ul>
	</div>
	 <img  class="ArrowLeft1"  src="slider-arrow-left.png " alt="arrow-left" style='display:none;'/>
	 <img class="ArrowRight1" src="slider-arrow-right.png " alt="arrow-right"/>
</div>
	</div>
		<script>
		$(document).ready(function(){
			var Slider1Count=($('#Slider1 ul li > img').length)
			var Slider1Width;
			var Counter1=1;
			var Width1=0;


			$('.ArrowLeft1').click(function(){
			Slider1Width=parseInt($('.con_trd_gallery').css('width'))
			if(Counter1>1)
			{
			$('.ArrowRight1').show();
			Width1=Width1+Slider1Width;
			Counter1=Counter1-1;
			$('#Slider1').animate({'left':Width1},500)
			if(1==Counter1)
			{
			$('.ArrowLeft1').hide();
			}
			}
			})
			$('.ArrowRight1').click(function(){
			Slider1Width=parseInt($('.con_trd_gallery').css('width'))
			//Slider1Width=parseInt(Slider1Width.substring(0,Slider1Width.length-2));
			if(Slider1Count>Counter1)
			{
			$('.ArrowLeft1').show();
			Width1=Width1-Slider1Width;
			Counter1=Counter1+1;
			$('#Slider1').animate({'left':Width1},500)
			if(Slider1Count==Counter1)
			{
			$('.ArrowRight1').hide();
			}
			}
			}) 

			})
			ProcessCount = '';
			whilesubmit = 0;	
			
			function urlfunction() {
				if (!(document.getElementById('Zap').checked == true
						|| document.getElementById('Nikto').checked == true || document
						.getElementById('Openssl').checked == true ||document.getElementById('Openvas').checked == true) ) {

					alert('Kindly select any one of checkbox');
					document.getElementById("Openssl").focus();
					return false;
				}
				if (document.getElementById('url').value == '') {
					alert('Kindly enter the URL');
					document.getElementById("url").focus();
					document.getElementById("url").select()
					return false;
				}
				if (!((/^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z0-9]{2,5}(:[a-z0-9]{1,5})?(\/.*)?$/
						.test(document.getElementById("url").value)) || (/^(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z0-9]{2,5}(:[a-z0-9]{1,5})?(\/.*)?$/
						.test(document.getElementById("url").value)))) {
					alert('Kindly enter correct URL');
					document.getElementById("url").focus();
					document.getElementById("url").select()
					return false;

				}
				
				if (document.getElementById('email').value == '') {
					alert('Kindly enter the email');
					document.getElementById("email").focus();
					document.getElementById("email").select()
					return false;
				}
				CommandText = '<p class="ref-class">Please,don\'t refresh the page.</p>';
				$('.wrapper').hide();
				$('#thankyou_content').show();
				document.getElementById('overlay-content').innerHTML = CommandText
						+ "<p><progress id='ProgressTag' value='1' max='100'></progress></p>";
				//SliderFunction();
				whilesubmit = 1;
				$('form').submit()
				return true;
				}
				function SliderFunction() {
				ProgressTool = "<div id='ToolText'>";
				
				$('.menus input:checked').each(
						function() {
							ProgressTool = ProgressTool + "<p>" + $(this).val()
									+ " Idle</p>";
						})
						
				ProgressTool = ProgressTool + "</div>";
				//$('#overlay-content-main').show();
				//$('#overlay-content').append(ProgressTool);
				ProgressVal = 100 / parseInt($('.menus input:checked').length)	
				setTimeout(function(){
						$('#ProgressTag').val(10);
				},10000)
				setInterval(function() {			
							$.ajax({url : 'session.jsp',
										success : function(data) {
											console.log(data)
											if (data == 0) {
												$('#ToolText p:eq('+ (data) + ')').html($('.menus input:checked:eq('+ (data)+ ')').val()+ ' Running...<span>&nbsp;</span>')
												
											} else {
											
												$('#ToolText p:eq('+ (data - 1)+ ')').html($('.menus input:checked:eq('+ (data - 1)+ ')').val()+ ' Completed<span>&nbsp;</span>').addClass('active')
												$('#ProgressTag').val(ProgressVal * data);
											}
											$('#ToolText p:eq(' + data + ')').html($('.menus input:checked:eq('+ data+ ')').val()+ ' Running...<span>&nbsp;</span>')

										},
										failure : function() {
											console.log("error")
										}
									})

						}, 50)
			}
			function enter_check(e){
				var e = window.event || e;
				var keyunicode = e.charCode || e.keyCode;
				if (keyunicode == 13) {
					if (!urlfunction()) {
						return false;
					}
				}
				return true;
			}
		
		</script>
	
<div id="bodysection">

		<div class="header">
			
				<img src="images/logo.png" alt="#" />
				<p style="float:right;">Logout</p>
		</div>
		<form method ='post' id='security_tool' name='security_tool'>
		<div class="content">
			<h1>
				A framework to secure your content and <br> protect your
				website
			</h1>
			
			<h4>Select tools from the below list to ensure appropriate
				protection for your website</h4>
			<div class="menus checkbox">
				<ul>
					<li><input type="checkbox" class="new" name="tools"
						id="Openssl" value="Openssl" data-val='2' checked /> <label for="Openssl">
							<img src="images/open-ssl.png" alt="#" title="Open SSL" />
					</label></li>
					<li><input type="checkbox" class="new" name="tools" id="Nikto"
						value="Nikto" data-val='1' checked /> <label for="Nikto"><img
							src="images/logo-3.png" alt="#" title="Nikto" /></label></li>
					<li><input type="checkbox" class="new" name="tools" id="Zap"
						value="Zap" data-val='0' checked /> <label for="Zap"><img
							src="images/logo-1.png" alt="#" title="OWASP ZAP" /></label></li>
					 <li>
						<input type="checkbox" class="new" id="Openvas"  name="tools" value="Openvas"  data-val='3' checked />
						<label  for="Openvas" ><img src="images/logo-2.png" alt="#" title="Open VAS" /></label>
					 </li> 

				</ul>
			</div>
			<div class="boxes">
				<input type="text" name="Name" id="url" title="" class="txtbox"
					placeholder=" http://" onkeypress='return enter_check(event)' />
				<input type="text" name="email" id="email" title="" class="txtbox2"
					placeholder=" Enter your mail id.."
					onkeypress='return enter_check(event)' /> 
					<input type="submit"
					name="submit" value="START SCAN" id="submit" title="" class="btn"
					onclick="return urlfunction();">

			</div>

			<div class="main_content">
				<div id="content1">
					<img src="images/diagram.png" usemap="#map" alt="" />
					<map name="map">
						<area shape="poly" href="javascript:void(0)" title="Nikto"
							coords="11,104,50,106,76,59,110,45,109,6,40,42"
							class="SF-riskmanagementSquare" alt="">
						<area class="SF-deliverySquare" shape="poly" title="OWASP ZAP"
							href="javascript:void(0)"
							coords="123,44,124,7,143,11,156,17,169,21,178,29,186,34,193,42,203,51,209,63,213,75,218,87,222,95,222,106,187,104,163,67"
							alt="">
						<area class="SF-candidateSquare" shape="poly" title="Open VAS"
							href="javascript:void(0)"
							coords="185,120,224,118,219,141,214,155,206,165,201,175,193,184,182,191,173,201,149,211,131,215,124,217,125,212,123,180,165,162,124,216,174,148"
							alt="">
						<area class="SF-testSquare" shape="poly" title="Open SSL"
							href="javascript:void(0)"
							coords="50,116,14,117,13,120,14,133,18,145,23,159,31,173,40,184,51,193,66,204,77,209,111,216,103,217,111,182,112,214,109,217,109,181,72,159"
							alt="">
						<area class="SF-holderCircle" shape="poly"
							title="OWASP Protection" href="javascript:void(0)"
							coords="70,117,81,80,110,65,134,67,153,78,165,95,170,112,166,128,156,143,145,155,122,163,100,158,78,139"
							alt="">
					</map>
				
				</div>

				<div class="content2">
					<div style="display: block;"
						class="SF-customerprotectionContent SF-callout wysiwyg">
						<div class="blockquoteRight callout">
							<p class="testcontent">&ldquo;A test security strategy must
								be three dimensional from a test, candidate and delivery
								perspective. To be effective, test owners must implement the
								appropriate risk management disciplines and a test security plan
								that align with their overall business strategy &rdquo;.</p>
						</div>
					</div>

					<div class="SF-riskmanagementContent wysiwyg" style="display: none">
						<h3 id="title1">NIKTO</h3>
						<p>Nikto is a Web server scanner that tests Web servers for
							dangerous files/CGIs, outdated server software and other
							problems. It performs generic and server type specific checks. It
							also captures and reports any cookies received.</p>
					</div>

					<div class="SF-deliveryContent wysiwyg" style="display: none;">
						<h3 id="title2">ZAP</h3>
						<p>The Zed Attack Proxy (ZAP) is an easy to use integrated
							penetration testing tool for finding vulnerabilities in web
							applications. It will detect the following top10 OWASP
							vulnerabilities.</p>
						<ul class="submenu">
							<li>Injection</li>
							<li>Broken Authentication and Session Management</li>
							<li>Cross-Site Scripting</li>
							<li>Insecure Direct Object References</li>
							<li>Cross Site Request Forgery</li>
						</ul>
					</div>

					<div class="SF-testContent wysiwyg" style="display: none;">
						<h3 id="title3">OPEN SSL</h3>
						<p>This free online service performs a deep analysis of the
							configuration of any SSL(Secure Socket Layer) web server on the
							public Internet. Please note that the information you submit here
							is used only to provide you the service. We don't use the domain
							names or the test results.</p>
					</div>

					<div class="SF-candidateContent wysiwyg" style="display: none;">
						<h3 id="title4">OPEN VAS</h3>
						<p>OpenVAS is a framework of several services and tools
							offering a comprehensive and powerful vulnerability scanning and
							vulnerability management solution.It protects against the
							following top10 OWASP attacks.</p>
						<ul class="submenu">
							<li>Using Components with Known Vulnerabilities</li>
							<li>Missing Function Level Access Control</li>
							<li>Security Misconfiguration</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	 
	</form>
</div>
</div>

<div id='thankyou_content' style='display:none;'>
<h1 class="thankyou">Thank You. Please give us a couple of hours to mail you the findings. </h1></div>
	<script type="text/javascript">
		$(document).ready(
				function($) {
					
					document.getElementById('security_tool').reset();
$('#check_again').click(function(){	
	location.reload();
})
					$(".SF-holderCircle")
							.click(
									function() {
										$(".SF-customerprotectionContent").css(
												'display', 'inline-block');
										$(".SF-riskmanagementContent").css(
												'display', 'none');
										$(".SF-deliveryContent").css('display',
												'none');
										$(".SF-candidateContent").css(
												'display', 'none');
										$(".SF-testContent").css('display',
												'none');
									});

					$(".SF-riskmanagementSquare")
							.click(
									function() {
										$(".SF-customerprotectionContent").css(
												'display', 'none');
										$(".SF-riskmanagementContent").css(
												'display', 'inline-block');
										$(".SF-deliveryContent").css('display',
												'none');
										$(".SF-candidateContent").css(
												'display', 'none');
										$(".SF-testContent").css('display',
												'none');
									});

					$(".SF-deliverySquare").click(
							function() {
								$(".SF-customerprotectionContent").css(
										'display', 'none');
								$(".SF-riskmanagementContent").css('display',
										'none');
								$(".SF-deliveryContent").css('display',
										'inline-block');
								$(".SF-candidateContent")
										.css('display', 'none');
								$(".SF-testContent").css('display', 'none');
							});

					$(".SF-candidateSquare")
							.click(
									function() {
										$(".SF-customerprotectionContent").css(
												'display', 'none');
										$(".SF-riskmanagementContent").css(
												'display', 'none');
										$(".SF-deliveryContent").css('display',
												'none');
										$(".SF-candidateContent").css(
												'display', 'inline-block');
										$(".SF-testContent").css('display',
												'none');
									});

					$(".SF-testSquare")
							.click(
									function() {
										$(".SF-customerprotectionContent").css(
												'display', 'none');
										$(".SF-riskmanagementContent").css(
												'display', 'none');
										$(".SF-deliveryContent").css('display',
												'none');
										$(".SF-candidateContent").css(
												'display', 'none');
										$(".SF-testContent").css('display',
												'inline-block');
									});

				});
	</script>
	<%
        	session.setAttribute("tools_exception", "");
  			session.setAttribute("theName", 0);
			String url = request.getParameter("Name");
			System.out.println(request.getAttribute("Name"));		
			String email = request.getParameter("email");
			System.out.println(url + "<<>>" + email);
			String[] tools = request.getParameterValues("tools");			
			if(tools!=null)
			{
				System.out.println("entered");
				new Tools_thread(tools,email,url,request, response,session);
			    Runnable runnable = new Tools_thread(tools,email,url,request, response,session);
				Thread t1 = new Thread(runnable);
				t1.start();
		 	    int thrd=0;    
		 	    do{
					if(t1.isAlive()==true){
				    
					
				   }
				else{
					System.out.println("dead"+session.getAttribute("theName"));
					String exception=session.getAttribute("tools_exception").toString();
					System.out.println(exception);
					if(exception.equalsIgnoreCase("")==true){
					}
					else
					{
					}thrd=1;
				
				}
				
			 	}while(thrd==0);		
			}				
	%>
</body>
</html>

