package com.integration.http;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Garmr implements Runnable{
	private static int      DISPLAY_NUMBER  = 99;
    private static String   XVFB            = "/usr/bin/Xvfb";
    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
    private static String   RESULT_FILENAME = "/tmp/screenshot.png";
    static Process p,q;
    HttpServletRequest request;
	HttpServletResponse response;
	HttpSession session;		    
	private final String email_parts;
	private final String url;
	public Garmr(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) {
		this.request=request;
		this.response=response;			
		this.email_parts=email_parts;
		this.url=url;
		this.session=session;
	}
	public void run(){
		try{
			session.setAttribute("validity_garmr", 1);
			Object value;
			String pid_value="";		 
			try{	
    		  Date date = new Date();          
    		  long timeMilli_garmr= date.getTime();
    		  double ran=Math.random();
    		  double timeMilli_garmr1=timeMilli_garmr*ran;     	         		  	       
    		  int  i=0;                		
              Runtime rt = Runtime.getRuntime();   
              String command="garmr -u "+url+" -o /home/qcuser/results/garmr/"+timeMilli_garmr1+".xml";        
              p=rt.exec(command);             
              Class<?> clazz = p.getClass();
              if (clazz.getName().equals("java.lang.UNIXProcess")) 
              {
  					Field pidField = clazz.getDeclaredField("pid");
  					pidField.setAccessible(true);
  					value = pidField.get(p);
  					if (value instanceof Integer) {
  						System.out.println("Detected pid: " + value);
  						pid_value=value.toString();
  				}      				
  			  }
              int j=0;
              File output=new File("/home/qcuser/results/garmr/"+timeMilli_garmr1+".xml");
              long prevLength=0;
              Thread.sleep(60000);
              long newlength=output.length();
               do {
				if (prevLength < newlength) 
				{
					prevLength = newlength;
					System.out.println("3");
					Thread.sleep(200000);
					newlength = output.length();
				} else 
				{
					j = 1;
					System.out.println("4");
				}
			  } while (j == 0);
              String output_file="/home/qcuser/results/garmr/"+timeMilli_garmr1+".xml";
			  String email_text = "Garmr - Vulnerability Report";
			  System.out.println(email_text);
			  SendMail.sendmail_1(output_file, email_parts, email_text);
			  System.out.println("DESTROYED");
			  p.destroy();
			  session.setAttribute("validity_garmr", 0);
			}
			catch(Exception e) {      	        	 	
    	 	System.out.println("catch garmr"); 
    	 	session.setAttribute("validity_garmr", 0);
    	 	p.destroy();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	}
