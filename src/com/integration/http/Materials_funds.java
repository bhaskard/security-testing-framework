package com.integration.http;

import java.io.File;
import java.io.FileOutputStream;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.integration.http.Readfile;


public class Materials_funds {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();


  @BeforeClass
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test(dataProvider="DP1")
  public void sgpdfname(String a,String b) throws Exception {
	  driver.get("http://gcbuat1:gcsggcbcbol1@2014@globalcommonbuild.uat1.citibank.com.sg/gcb/investments/id_uts.htm");
	  Thread.sleep(2000);
	  driver.findElement(By.linkText("FUND MATERIALS")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.className("sbToggle")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.linkText("Fund Details and Performance Update")).click();
	  driver.findElement(By.id("fundPerformance")).click();
	  driver.findElement(By.id("fundPerformance")).clear();
	  driver.findElement(By.id("fundPerformance")).sendKeys(a);
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id='perform']")).click();
	  Thread.sleep(15000);
	  
	  Readfile fileobj=new Readfile();
	  String checkfile=fileobj.checkfilename();
	 
	  
	 System.out.println("checkfile  "+checkfile);
	 Thread.sleep(2000);
	 Assert.assertEquals(checkfile, b);
	 /*if(checkfile.equals(b)){
		 System.out.println(checkfile+b+" = sucess");
	 }else{
		 System.out.println(checkfile+b+" = Wrong pdf");
	 }*/
	 
	 Readfile filedel = new Readfile();
	filedel.deletefilename();
	 
	
      }
      
  
	

@AfterClass
  public void tearDown() throws Exception {
    driver.quit();
}
    @DataProvider(name = "DP1")
    public Object[][] createData1() throws Exception{
        Object[][] retObjArr=getTableArray("D:/factsheet.xls","Sheet1","dp1");
        return(retObjArr);
    }
        public String[][] getTableArray(String xlFilePath, String sheetName, String tableName) throws Exception{
            String[][] tabArray=null;
           
                Workbook workbook = Workbook.getWorkbook(new File(xlFilePath));
                Sheet sheet = workbook.getSheet(sheetName);
                int startRow,startCol, endRow, endCol,ci,cj;
                Cell tableStart=sheet.findCell(tableName);
                startRow=tableStart.getRow();
                startCol=tableStart.getColumn();

                Cell tableEnd= sheet.findCell(tableName, startCol+1,startRow+1, 100, 64000,  false);                          

                endRow=tableEnd.getRow();
                endCol=tableEnd.getColumn();
                System.out.println("startRow="+startRow+", endRow="+endRow+", " +
                        "startCol="+startCol+", endCol="+endCol);
                tabArray=new String[endRow-startRow-1][endCol-startCol-1];
                ci=0;

                for (int i=startRow+1;i<endRow;i++,ci++){
                    cj=0;
                    for (int j=startCol+1;j<endCol;j++,cj++){
                        tabArray[ci][cj]=sheet.getCell(j,i).getContents();
                    }
                }
           

            return(tabArray);
        }
        
        //write pdf name to excel
        
}
