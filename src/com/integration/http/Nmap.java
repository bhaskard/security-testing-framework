package com.integration.http;

import javax.servlet.http.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


//import bsh.Parser;
import javax.xml.*;

import com.integration.http.SendMail;

import java.lang.reflect.Field;
import java.io.File;
import java.io.IOException;
import java.util.Date;
public class Nmap implements Runnable {	
		private static int      DISPLAY_NUMBER  = 99;
	    private static String   XVFB            = "/usr/bin/Xvfb";
	    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
	    private static String   RESULT_FILENAME = "/tmp/screenshot.png";
	    static Process p,q;
	    //public static  void Nmap_method(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) throws IOException{
	    HttpServletRequest request;
		HttpServletResponse response;
		HttpSession session;
	    
		private final String email_parts;
		private final String url;
		public Nmap(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) {
			this.request=request;
			this.response=response;
		
			this.email_parts=email_parts;
			this.url=url;
			this.session=session;
		}
		public void run(){
			try{
				session.setAttribute("validity_ssl", 1);
		Object value;
		String pid_value="";		 
        try{	
        		  Date date = new Date();          
        		  long timeMilli_nmap= date.getTime();
        		  double ran=Math.random();
        		  double timeMilli_nmap1=timeMilli_nmap*ran;     	         		  	       
        		  int  i=0;                		
                  Runtime rt = Runtime.getRuntime();   
              //    String str="nmap -sS -sU -T4 -A -v -PE -PP -PS80,443 -PA3389 -PU40125 -PY -g 53 –script \“discovery and safe\” "+url+"  && xsltproc /home/qcuser/results/nmap/report_"+timeMilli_nmap1+".xml -o /home/qcuser/results/nmap/report_"+timeMilli_nmap1+".html";
                  String command="nmap -sS -sU -T4 -A -v -PE -PP -PS80,443 -PA3389 -PU40125 -PY -g 53 –script default or \\(discovery and safe\\) "+url+" -oX /home/qcuser/results/nmap/report_"+timeMilli_nmap1+".xml  && xsltproc /home/qcuser/results/nmap/report_"+timeMilli_nmap1+".xml -o /home/qcuser/results/nmap/report_"+timeMilli_nmap1+".html";
                  //   String[] cmdArray={"/usr/bin/lxterminal","-e",command,"-accept=socket,host=localhost,port=8100;urp;", "-invisible", "-nologo"};
                  //  String command="/home/qcuser/ZAP_2.3.1/zap.sh -daemon -newsession /home/qcuser/results/zap/"+ran+" -quickurl "+url+" -last_scan_report /home/qcuser/results/zap/report_"+timeMilli_zap1+".html";
                      
                  p=rt.exec(command);
                  System.out.println("5");
                  Class<?> clazz = p.getClass();
                  if (clazz.getName().equals("java.lang.UNIXProcess")) 
                  {
      					Field pidField = clazz.getDeclaredField("pid");
      					pidField.setAccessible(true);
      					value = pidField.get(p);
      					if (value instanceof Integer) {
      						System.out.println("Detected pid: " + value);
      						pid_value=value.toString();
      				}      				
      			  }
                  int j=0;
                  
                  System.out.println(command);			
            	  File output = new File("/home/qcuser/results/nmap/report_"+ timeMilli_nmap1 +".html");
    			  Thread.sleep(50000);		 		
    			  do{
    				  if(output.exists()==true){
    					  Document htmlFile = null;
    					  htmlFile = Jsoup.parse(new File("/home/qcuser/results/nmap/report_"+ timeMilli_nmap1 + ".html"), "ISO-8859-1");
    					  
    					  if(htmlFile.toString().contains("Nmap done")==true)
    					  {
    					  System.out.println("finished nmap");
    					  j=1;
    				  }
    				  else{
    					  System.out.println("not finished nmap");    			 		
    				  } 
    				  }
    				  Thread.sleep(80000);	
    			  }while(j==0);		 
    			 
    			  System.out.println("OUT OF nmap");
    			  Thread.sleep(10000);
    			  String command_1="xsltproc /home/qcuser/results/nmap/report_"+timeMilli_nmap1+".xml -o /home/qcuser/results/nmap/report_"+timeMilli_nmap1+".html";
    			  q=rt.exec(command_1);
    			  Thread.sleep(10000);
    			  File output_1 = new File("/home/qcuser/results/nmap/report_"+ timeMilli_nmap1 +".html");
    			  String output_file = output_1.toString();
    			  String email_text = "Nmap - Vulnerability Report";
    			  System.out.println(email_text);
    			  SendMail.sendmail_1(output_file, email_parts, email_text);
    			  System.out.println("DESTROYED");
    			  p.destroy();
    			  session.setAttribute("validity_ssl", 0);
              		//		proc.destroy();
              		//	int message=Integer.parseInt(session.getAttribute("theName").toString())+1;
              		//  	String error="true";
              		//	  	session.setAttribute("error", error);
        	}
        catch(Exception e) {      	        	 	
        	 	System.out.println("catch nmap"); 
        	 	session.setAttribute("validity_ssl", 0);
        	 	p.destroy();

         }
	}catch(Exception e){
		e.printStackTrace();
	}
	}
	}
