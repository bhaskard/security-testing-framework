package com.integration.http;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import com.integration.http.xls_read;
import com.integration.http.constant;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class Openvas {
	private static int      DISPLAY_NUMBER  = 99;
    private static String   XVFB            = "/usr/bin/Xvfb";
    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
	public static void openvas(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) throws IOException,Exception{
		Process p = Runtime.getRuntime().exec(XVFB_COMMAND);
        FirefoxBinary firefox = new FirefoxBinary();
        firefox.setEnvironmentProperty("DISPLAY", ":" + DISPLAY_NUMBER);      
		final String path=constant.Path_TestData;
		final String SheetName=constant.SheetName;
		String username="";
		String password="";
		String Result="";
		int buffer=0;
	    ProfilesIni profile = new ProfilesIni();	
		String download_path = "/home/qcuser/Downloads";
		FirefoxProfile myprofile = profile.getProfile("security_frame");
		myprofile.setPreference("browser.download.folderList", 2);
		myprofile.setPreference("browser.download.dir", download_path);
		myprofile.setPreference("browser.download.manager.showWhenStarting",false);
		myprofile.setPreference("browser.download.alertOnEXEOpen", false);
		myprofile.setPreference("browser.helperApps.neverAsksaveToDisk", "application/pdf,application/x-pdf");
		myprofile.setPreference("browser.download.manager.focusWhenStarting", false);
		myprofile.setPreference("browser.helperApps.alwaysAsk.force", false);
		myprofile.setPreference("browser.download.manager.alertOnEXEOpen", false);
		myprofile.setPreference("browser.download.manager.closeWhenDone", false);
		myprofile.setPreference("browser.download.manager.showAlertOnComplete", false);
		myprofile.setPreference("browser.download.manager.useWindow", false);
		myprofile.setPreference("browser.download.manager.showWhenStarting", false);
		myprofile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);		
		WebDriver driver = new FirefoxDriver(firefox, myprofile);
		try{
			xls_read.xl_read_1(path, SheetName);
			System.out.println(path);
			System.out.println(SheetName);
			System.out.print("1");
			int iTestCaseRow;
		
			for(iTestCaseRow=0;iTestCaseRow<11;iTestCaseRow++){
				if((xls_read.getCellData(SheetName,iTestCaseRow,constant.status).toString()).equalsIgnoreCase("on")==true){
				username=xls_read.getCellData(SheetName,iTestCaseRow,constant.username);
				password=xls_read.getCellData(SheetName,iTestCaseRow,constant.password);
				buffer=iTestCaseRow;
				Result="off";
				xls_read.setCellData(Result, iTestCaseRow, 2,path,SheetName);
				iTestCaseRow=13;
				}
			}
				WebDriverWait wait_next = new WebDriverWait(driver, 400000);
				System.out.println(username+"<<>>"+password);
				driver.get("https://siteaudit.xerago.com/login/login.html");
			    driver.manage().timeouts().implicitlyWait( 60,TimeUnit.SECONDS);
				driver.findElement(By.xpath("html/body/center/div/div/div[4]/center/form/table/tbody/tr[1]/td[2]/input")).sendKeys(username);
				driver.findElement(By.xpath("html/body/center/div/div/div[4]/center/form/table/tbody/tr[2]/td[2]/input")).sendKeys(password);
				driver.findElement(By.xpath("html/body/center/div/div/div[4]/center/form/div/input")).click();
				int len=driver.getCurrentUrl().length();
				int first_index_1=driver.getCurrentUrl().indexOf("token=");
				first_index_1=first_index_1+6;
				System.out.println("outrage");	
				String substr=driver.getCurrentUrl().substring(first_index_1, len);	
				driver.get("https://siteaudit.xerago.com/omp?cmd=wizard&name=quick_first_scan&refresh_interval=30&filter=&filt_id=&token="+substr);
			    System.out.println("outrage");
			    driver.findElement(By.xpath("html/body/center/div/div[5]/table/tbody/tr/td[3]/form/input[10]")).sendKeys(url);
			    driver.findElement(By.xpath("html/body/center/div/div[5]/table/tbody/tr/td[3]/form/input[11]")).click();
			    driver.findElement(By.xpath("//*[@id='gb_menu']/ul/li[1]/a")).click();
			    System.out.println("outrage");
			    driver.findElement(By.xpath("//*[@id='gb_menu']/ul/li[1]/ul/li[3]/a")).click();
			    System.out.println("outrage_1");
			    driver.navigate().refresh();
			    driver.navigate().refresh();
			    driver.navigate().refresh();
			    driver.navigate().refresh();
			    driver.navigate().refresh();
			    int i=0;
			    driver.findElement(By.xpath("html/body/center/div/div[2]/div[5]/div/table/tbody/tr[3]/td[1]/b/a")).click();
			    do{
			    	System.out.println(driver.findElement(By.xpath("html/body/center/div/div[2]/div[3]/div[2]/a/div/div[2]")).getText().toString());
			    	if((driver.findElement(By.xpath("html/body/center/div/div[2]/div[3]/div[2]/a/div/div[2]")).getText().toString().equalsIgnoreCase("Done"))==true){
			    		i=1;
			    		System.out.println("got loop");
			    	}
			    	driver.navigate().refresh();
			    }while(i==0);
			    wait_next.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Done"))).isDisplayed();  
			    driver.findElement(By.xpath("//*[@id='small_inline_form']/form/input[16]")).click();
			    Thread.sleep(5000);
			    String final_url=driver.getCurrentUrl();
			    System.out.println(final_url);
				int first_index=final_url.indexOf("report_id=");
				first_index=first_index+10;
				int last_index=final_url.indexOf("&notes");
				System.out.println(final_url.substring(first_index, last_index));
				String report_name=final_url.substring(first_index, last_index);
				report_name="report-"+report_name+".pdf";
				System.out.println(report_name);
				File output=new File("/home/qcuser/Downloads/"+report_name);
				String output_file=output.toString();
	            do{
	            	if(output.exists()==true){
	            		System.out.print("created");
	            		i=1;
	            	}     
	            	else
	            	{
	            		Thread.sleep(200000);
	            		System.out.println("not finished openvas"+url);
	            	}
	            }while(i==0);
	            String email_text="OPENVAS - Vulnerability Report";
	            SendMail.sendmail_1(output_file,email_parts,email_text);  
	            driver.findElement(By.xpath("html/body/center/div/div[1]/div[3]/div[1]/a")).click();	            
	            driver.quit();	             
	            System.out.println("buffer"+buffer);
	            Result="on";
	            xls_read.setCellData(Result, buffer, 2,path,SheetName);			   	 
			   	System.out.println("end");	
			    p.destroy();
				}
				catch(Exception e){
					Result="on";
					System.out.println("buffer"+buffer);    
					String tool="openvas";
					xls_read.setCellData(Result, buffer, 2,path,SheetName);	       
					driver.quit();
					e.printStackTrace();
					p.destroy();
					System.out.println("end exception");
	  	//	if(session.getAttribute("tools_exception").toString().equalsIgnoreCase("")!=true)
	  	//	{
	  	//		String exception=tool+","+session.getAttribute("tools_exception").toString();
	  	//		session.setAttribute("tools_exception",exception);
	  	//	}
	  	//	else{
	  		
	  	//		session.setAttribute("tools_exception",tool);
	  	//	}
	        
		//	 int message=Integer.parseInt(session.getAttribute("theName").toString())+1;
 		 //  	 session.setAttribute("theName", message);	
		}
	}
}
