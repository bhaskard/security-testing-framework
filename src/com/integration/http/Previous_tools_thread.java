package com.integration.http;

import com.integration.http.nikto;

import org.apache.commons.lang.StringUtils;

import com.integration.http.zap;
import com.integration.http.openssl;

import java.io.*;
import java.sql.ResultSet;

import org.apache.commons.jxpath.servlet.ServletContextHandler;

import java.net.*;

import javax.servlet.http.*;
import javax.servlet.*;
import javax.mail.Session;

public class Previous_tools_thread implements Runnable  {	
		HttpServletRequest request;
		HttpServletResponse response;
		HttpSession session;
		private final String[] tools;
		private final String email;
		private final String url;
		private  int numofTools=0;
		public Previous_tools_thread(String[] tools,String email,String url,HttpServletRequest request,HttpServletResponse response,HttpSession session) {
			this.request=request;
			this.response=response;
			this.tools = tools;
			this.email=email;
			this.url=url;
			this.session=session;
		}
		public void run() 
		{			
		try{			
			if (tools != null) 
			{			
				numofTools = tools.length;
				for (int k = 0; k < numofTools; k++) 
				{
					String tool_name = tools[k];					
					if (url != null
							&&(tool_name.equalsIgnoreCase("Openssl") == true))
					{
						System.out.println("on openssl"+session.getAttribute("theName"));
						System.out.print("Openssl processing...");
						openssl.opnssl_check(url, email, response, request,session);					
					}					
					if (url != null
							&& (tool_name.equalsIgnoreCase("Openvas") == true)){
						String url_final;
						url_final=url;
						if (((url.contains("http://")) == false)
								&& ((url.contains("https://")) == false)) 
						{							
									StringBuffer sb = new StringBuffer("http://");
									 url_final = sb.append(url).toString();
									
						} 						
						URL url_new=new URL(url_final);
						System.out.println(url_new.getHost());
						url_final=url_new.getHost();
						if ((url_final.contains("www")) == true)			 
						{							
							String url_str = "www.";
							url_final = url_final.replace(url_str,"");
							
						} 
						System.out.println("url_final"+url_final);
						Nmap.Nmap_method(url_final, email, response, request, session);
						//Openvas.openvas(url_final, email, response, request, session);	
					}									
					if (url != null
							&& (tool_name.equalsIgnoreCase("Zap") == true))
					{
						String url_final;
						
						url_final=url;
						if ((url.contains("www")) == true)			 
						{							
							String url_str = "www.";
							url_final = url_final.replace(url_str,"");
							
						}
						if (((url_final.contains("http://")) == false)
								&& ((url_final.contains("https://")) == false)) 
						{							
									StringBuffer sb = new StringBuffer("http://");
									String url_defined = sb.append(url).toString();
									zap.Zap1(url_defined, email, response, request,session);
						} 
						else 
						{
									System.out.print("zap processing...");
									zap.Zap1(url_final, email, response, request,session);

						}
					}					
					if (url != null
							&& (tool_name.equalsIgnoreCase("Nikto") == true)){
						String url_final;
						url_final=url;
						if (((url_final.contains("http://")) == false)
								&& ((url_final.contains("https://")) == false))
						{							
									StringBuffer sb = new StringBuffer("http://");
									url_final = sb.append(url_final).toString();
									
									}
						
						URL url_new=new URL(url_final);
						System.out.println(url_new.getHost());
						url_final=url_new.getHost();
						
						if(url_final.contains("www.")==false)
						{
							StringBuffer sb = new StringBuffer("www.");
							url_final = sb.append(url_final).toString();
							//String url_str = "www.";
							//url_final = url.replace(url_str,"");
						}
						int count = StringUtils.countMatches(url_final, ".");
						System.out.println(count);
						if(count>=3){
							if(url_final.contains("www.")==true)
							{
								String url_str = "www.";
								url_final = url_final.replace(url_str,"");
							}
							
						}
						if (((url_final.contains("http://")) == false)
								&& ((url_final.contains("https://")) == false))
						{							
									StringBuffer sb = new StringBuffer("http://");
									url_final = sb.append(url_final).toString();
									System.out.println(url_final);
									}
						nikto.Nikto1(url_final, email, response, request,session);
					}
					
					/*{	
						String url_final;
						url_final=url;
						int count = StringUtils.countMatches(url_final, ".");
						if((count>=3)&&(((url_final.contains("http://")) == true)
								&& ((url_final.contains("https://")) == true))){
							if(url_final.contains("www.")==true)
							{
								String url_str = "www.";
								url_final = url.replace(url_str,"");
							}
						}
						else{
						
						if(url_final.contains("www.")==false)
						{
							StringBuffer sb = new StringBuffer("www.");
							url_final = sb.append(url_final).toString();
							//String url_str = "www.";
							//url_final = url.replace(url_str,"");
						}
						}
						if (((url_final.contains("http://")) == false)
								&& ((url_final.contains("https://")) == false))
						{							
									StringBuffer sb = new StringBuffer("http://");
									url_final = sb.append(url_final).toString();
									System.out.print("nikto processing...");
									System.out.println(url_final);
									nikto.Nikto1(url_final, email, response, request,session);
						}
						else
						{
							System.out.println(url_final);
							nikto.Nikto1(url_final, email, response, request,session);
						}
					}*/
		
				}
			}
		} 
		catch(Exception e){
			e.printStackTrace();
			System.out.println("catched");
			String error="false";
		  //	session.setAttribute("error", error);
		}

		}

		}
	

	


