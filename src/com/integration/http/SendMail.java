package com.integration.http;
import java.util.Properties;
import javax.activation.*;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMail{

public static void sendmail_1(String fileName, String email, String text) throws MessagingException {		
	String host = "xmail.xerago.com";
	final String username = "siteaudit@xerago.com";
    final String password = "786@Xerago";
    Properties props = new Properties();
    props.put("mail.smtp.auth",true); 
    props.put("mail.smtp.starttls.enable", true);
    props.put("mail.smtp.ssl.trust", "*");
    props.put("mail.smtp.port", "587");
    props.setProperty("mail.smtp.host", host);
    props.put("mail.smtp.ssl.enable", false);
  //  props.put("mail.smtp.starttls.enable", false);
    Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

    try {
    	   Message message = new MimeMessage(session);
           message.setFrom(new InternetAddress("siteaudit@xerago.com"));
           message.setRecipients(Message.RecipientType.TO,
                   InternetAddress.parse(email));
           message.setSubject(text);
           MimeBodyPart messageBodyPart = new MimeBodyPart();
           messageBodyPart.setContent("<p>Hello,<br/><br/> Thanks for giving us the time we requested, you can now refer attached report which takes you through the Vulnerability issues we found.<br/><br/> Have a Great day.!!!<br/>VA Team - Xerago.<br/><br/> <span style='color:#686868;'>Please write to <a href='mailto:siteaudit@xerago.com'>siteaudit@xerago.com</a>, if any improvements or bug if you come across. We guarantee you that we will work on all your comments and make the system more comprehensive as you all required.</p>",
           	        	                "text/html" );
           Multipart multipart = new MimeMultipart();
           multipart.addBodyPart(messageBodyPart);
           messageBodyPart = new MimeBodyPart(); 
           DataSource source = new FileDataSource(fileName);     
           messageBodyPart.setDataHandler(new DataHandler(source));       
           messageBodyPart.setFileName(fileName);        
           message.setFileName(fileName);
           multipart.addBodyPart(messageBodyPart);
           message.setContent(multipart);
           
           System.out.println("Sending"+email+"<<>>"+fileName+"<<>>"+text);
           Transport.send(message);
           System.out.println("Done");

    } catch (MessagingException e) {
    	System.out.println("sending failed");
        e.printStackTrace();
    }
  }
	
}