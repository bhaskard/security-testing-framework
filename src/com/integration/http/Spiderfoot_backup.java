package com.integration.http;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.collect.ImmutableMap;

//public class Spiderfoot implements Runnable{
public class Spiderfoot_backup implements Runnable{	
private static int      DISPLAY_NUMBER  = 99;
    private static String   XVFB            = "/usr/bin/Xvfb";
    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
   
    static Process p,q;
    HttpServletRequest request;
	HttpServletResponse response;
	HttpSession session;		    
	private final String email_parts;
	private final String url;
	public Spiderfoot_backup(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) {
		this.request=request;
		this.response=response;			
		this.email_parts=email_parts;
		this.url=url;
		this.session=session;
	}
	//public static void main(String args[]){
	public void run(){
		double timeMilli_spiderfoot;
		try{
			 Date date = new Date();    
			  long timeMilli= date.getTime();
			  double ran=Math.random();
			  timeMilli_spiderfoot=timeMilli*ran;
			session.setAttribute("validity_spiderfoot", 1);
			Process p = Runtime.getRuntime().exec(XVFB_COMMAND);
				FirefoxBinary firefox = new FirefoxBinary();
			firefox.setEnvironmentProperty("DISPLAY", ":" + DISPLAY_NUMBER);  
			System.out.println("starting browser");
			ProfilesIni profile = new ProfilesIni();
			String download_path = "/home/qcuser/results/spiderfoot/"+timeMilli_spiderfoot;
			FirefoxProfile myprofile = profile.getProfile("load_testing");
			myprofile.setPreference("browser.download.folderList", 2);
			myprofile.setPreference("browser.download.dir", download_path);
			myprofile.setPreference("browser.download.manager.showWhenStarting",false);
			myprofile.setPreference("browser.download.alertOnEXEOpen", false);
			myprofile.setPreference("browser.helperApps.neverAsksaveToDisk", "application/pdf,application/x-pdf");
			myprofile.setPreference("browser.download.manager.focusWhenStarting", false);
			myprofile.setPreference("browser.helperApps.alwaysAsk.force", false);
			myprofile.setPreference("browser.download.manager.alertOnEXEOpen", false);
			myprofile.setPreference("browser.download.manager.closeWhenDone", false);
			myprofile.setPreference("browser.download.manager.showAlertOnComplete", false);
			myprofile.setPreference("browser.download.manager.useWindow", false);
			myprofile.setPreference("browser.download.manager.showWhenStarting", false);
			myprofile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);	
			System.out.println("starting 1");
			WebDriver driver = new FirefoxDriver(firefox, myprofile);	
			System.out.println("starting 2");
			driver.get("http://127.0.0.1:5001/");
			System.out.println("starting 2");
			driver.findElement(By.xpath("html/body/div[1]/div/div/div/ul[1]/li[1]/a")).click();
			System.out.println("starting 2");
			String scanname=timeMilli_spiderfoot+"";
			Thread.sleep(60000);
			driver.findElement(By.xpath("//*[@id='scanname']")).sendKeys(scanname);
			System.out.println("starting 2");
			driver.findElement(By.xpath("//*[@id='scantarget']")).sendKeys(url);
			driver.findElement(By.xpath("//*[@id='mainbody']/form/div[4]/div/button")).click();					
			  String email_text = "SpiderFoot - Vulnerability Report";
			  System.out.println(email_text);
			  int j=0;
			  do{
			 if( driver.findElement(By.xpath("//*[@id='status']")).getText().equalsIgnoreCase("FINISHED")){
				 j=1;
			 }
			  }	while(j==0);
			  driver.findElement(By.xpath("//*[@id='btn-browse']")).click();	
				driver.findElement(By.xpath("//*[@id='btn-export']")).click();				
				File file_output=new File("/home/qcuser/results/spiderfoot/"+timeMilli_spiderfoot+"/SpiderFoot.csv");
				int out=0;
				  do{
				if(file_output.exists()){
					out=1;
				 }
				  }	while(out==0);
				  Thread.sleep(10000);
				  String output_file="/home/qcuser/results/spiderfoot/"+timeMilli_spiderfoot+"/SpiderFoot.csv";
				  System.out.println(email_text);
				  SendMail.sendmail_1(output_file, email_parts, email_text);
				  System.out.println("DESTROYED");
				  session.setAttribute("validity_spiderfoot", 0);
				  p.destroy();	
			}
			catch(Exception e) {      	        	 	
    	 	System.out.println("catch spiderfoot"); 
    	 	session.setAttribute("validity_spiderfoot", 0);
    	 	p.destroy();

			}
	}

}
    

