package com.integration.http;

import com.integration.http.nikto;
import com.integration.http.Wapiti;
import org.apache.commons.lang.StringUtils;

import com.integration.http.zap;
import com.integration.http.openssl;

import java.io.*;
import java.sql.ResultSet;

import org.apache.commons.jxpath.servlet.ServletContextHandler;

import java.net.*;

import javax.servlet.http.*;
import javax.servlet.*;
import javax.mail.Session;

public class Tools_thread implements Runnable  {	
		HttpServletRequest request;
		HttpServletResponse response;
		HttpSession session;
		private final String[] tools;
		private final String email;
		private final String url;
		private  int numofTools=0;
		public Tools_thread(String[] tools,String email,String url,HttpServletRequest request,HttpServletResponse response,HttpSession session) {
			this.request=request;
			this.response=response;
			this.tools = tools;
			this.email=email;
			this.url=url;
			this.session=session;
		}
		public void run() 
		{			
		try{			
			if (tools != null) 
			{	Thread t1,t2,t3,t4,t5,t6,t7,t8,t9,t10;
				session.setAttribute("validity_ssl", 0);
				session.setAttribute("validity_nmap", 0);				
				session.setAttribute("validity_zap", 0);
				session.setAttribute("validity_nikto", 0);
				session.setAttribute("validity_wapiti", 0);
				session.setAttribute("validity_wpscan", 0);
				session.setAttribute("validity_skipfish", 0);
				session.setAttribute("validity_garmr", 0);
				session.setAttribute("validity_uniscan", 0);
				session.setAttribute("validity_spiderfoot", 0);
				session.setAttribute("validity_golismero", 0);
				numofTools = tools.length;
				for (int k = 0; k < numofTools; k++) 
				{
					String tool_name = tools[k];					
					if (url != null
							&&(tool_name.equalsIgnoreCase("Openssl") == true))
					{
						String url_final;
						url_final=url;
						if (((url.contains("http://")) == false)
								&& ((url.contains("https://")) == false)) 
						{							
									StringBuffer sb = new StringBuffer("http://");
									 url_final = sb.append(url).toString();
									
						} 						
						URL url_new=new URL(url_final);
						System.out.println(url_new.getHost());
						url_final=url_new.getHost();
						if ((url_final.contains("www")) == true)			 
						{							
							String url_str = "www.";
							url_final = url_final.replace(url_str,"");							
						} 
						System.out.println("url_final"+url_final); 
					//	nmap_ssl.NmapSSL_method(url_final, email, response, request, session);
						
						new nmap_ssl(url_final, email, response, request, session);
					    Runnable runnable = new nmap_ssl(url_final, email, response, request, session);
						t1 = new Thread(runnable);
						t1.start();
						
						
						
					}	
					
					
					if (url != null
							&& (tool_name.equalsIgnoreCase("Openvas") == true)){
						String url_final;
						url_final=url;
						if (((url.contains("http://")) == false)
								&& ((url.contains("https://")) == false)) 
						{							
									StringBuffer sb = new StringBuffer("http://");
									 url_final = sb.append(url).toString();
									
						} 						
						URL url_new=new URL(url_final);
						System.out.println(url_new.getHost());
						url_final=url_new.getHost();
						if ((url_final.contains("www")) == true)			 
						{							
							String url_str = "www.";
							url_final = url_final.replace(url_str,"");
							
						} 
						System.out.println("url_final"+url_final);
				//		Nmap.Nmap_method(url_final, email, response, request, session);
						
						new Nmap(url_final, email, response, request, session);
					    Runnable runnable = new Nmap(url_final, email, response, request, session);
						t2 = new Thread(runnable);
						t2.start();
						
						
					}									
					if (url != null
							&& (tool_name.equalsIgnoreCase("Zap") == true))
					{
						String url_final;
						url_final=url;
						/*if ((url.contains("www")) == true)			 
						{							
							String url_str = "www.";
							url_final = url_final.replace(url_str,"");
							
						}*/
						if (((url_final.contains("http://")) == false)
								&& ((url_final.contains("https://")) == false)) 
						{							
									StringBuffer sb = new StringBuffer("http://");
									String url_defined = sb.append(url).toString();
								//	zap.Zap1(url_defined, email, response, request,session);
									new zap(url_defined, email, response, request, session);
								    Runnable runnable = new zap(url_defined, email, response, request, session);
									t3 = new Thread(runnable);
									t3.start();
						} 
						else 
						{
									System.out.print("zap processing...");
								//	zap.Zap1(url_final, email, response, request,session);
									new zap(url_final, email, response, request, session);
								    Runnable runnable = new zap(url_final, email, response, request, session);
									t3 = new Thread(runnable);
									t3.start();

						}
					}					
					if (url != null
							&& (tool_name.equalsIgnoreCase("Nikto") == true)){
						String url_final;
						url_final=url;
						if (((url_final.contains("http://")) == false)
								&& ((url_final.contains("https://")) == false))
						{							
									StringBuffer sb = new StringBuffer("http://");
									url_final = sb.append(url_final).toString();
									
									}
						
						URL url_new=new URL(url_final);
						System.out.println(url_new.getHost());
						url_final=url_new.getHost();
						if(url_final.contains("www.")==true)
						{
							url_final=url_final.replace("www.","");
							//String url_str = "www.";
							//url_final = url.replace(url_str,"");
						}
						
					/*	
						if(url_final.contains("www.")==false)
						{
							StringBuffer sb = new StringBuffer("www.");
							url_final = sb.append(url_final).toString();
							//String url_str = "www.";
							//url_final = url.replace(url_str,"");
						}
						int count = StringUtils.countMatches(url_final, ".");
						System.out.println(count);
						if(count>=3){
							if(url_final.contains("www.")==true)
							{
								String url_str = "www.";
								url_final = url_final.replace(url_str,"");
							}
							
						}
					*/
					//	nikto.Nikto1(url_final, email, response, request,session);
						
						new nikto(url_final, email, response, request, session);
					    Runnable runnable = new nikto(url_final, email, response, request, session);
						t4 = new Thread(runnable);
						t4.start();
						
						
						
					}
					if (url != null
							&& (tool_name.equalsIgnoreCase("Wapiti") == true))
					{
						String url_final;
						url_final=url;
					//	Wapiti.Wapiti_scan(url_final, email, response, request, session);						
						new Wapiti(url_final, email, response, request, session);
					    Runnable runnable = new Wapiti(url_final, email, response, request, session);
						t5 = new Thread(runnable);
						t5.start();	
					}	
					if (url != null
							&& (tool_name.equalsIgnoreCase("Wpscan") == true))
					{
						String url_final;
						url_final=url;
					//	Wpscan.Wpscan_scan(url_final, email, response, request, session);
						new Wpscan(url_final, email, response, request, session);
					    Runnable runnable = new Wpscan(url_final, email, response, request, session);
						t6 = new Thread(runnable);
						t6.start();
						
					}
					if (url != null
							&& (tool_name.equalsIgnoreCase("Skipfish") == true))
					{
						String url_final;
						url_final=url;
						new Skipfish(url_final, email, response, request, session);
					    Runnable runnable = new Skipfish(url_final, email, response, request, session);
						t7 = new Thread(runnable);
						t7.start();
						
					}
					
					if (url != null
							&& (tool_name.equalsIgnoreCase("Garmr") == true))
					{
						String url_final;
						url_final=url;
						new Garmr(url_final, email, response, request, session);
					    Runnable runnable = new Garmr(url_final, email, response, request, session);
						t8 = new Thread(runnable);
						t8.start();
						
					}
					
					if (url != null
							&& (tool_name.equalsIgnoreCase("Uniscan") == true))
					{
						String url_final;
						url_final=url;
						
						new Golismero(url_final, email, response, request, session);
					    Runnable runnable = new Golismero(url_final, email, response, request, session);
						t9 = new Thread(runnable);
						t9.start();	
					}
					
					if (url != null
							&& (tool_name.equalsIgnoreCase("Spiderfoot") == true))
					{
						
						String url_final;
						url_final=url;
						URL url_new=new URL(url_final);
						System.out.println(url_new.getHost());
						url_final=url_new.getHost();
						new Spiderfoot(url_final, email, response, request, session);
					    Runnable runnable = new Spiderfoot(url_final, email, response, request, session);
						t10 = new Thread(runnable);
						t10.start();
						
					}
					
					/*{	
						String url_final;
						url_final=url;
						int count = StringUtils.countMatches(url_final, ".");
						if((count>=3)&&(((url_final.contains("http://")) == true)
								&& ((url_final.contains("https://")) == true))){
							if(url_final.contains("www.")==true)
							{
								String url_str = "www.";
								url_final = url.replace(url_str,"");
							}
						}
						else{
						
						if(url_final.contains("www.")==false)
						{
							StringBuffer sb = new StringBuffer("www.");
							url_final = sb.append(url_final).toString();
							//String url_str = "www.";
							//url_final = url.replace(url_str,"");
						}
						}
						if (((url_final.contains("http://")) == false)
								&& ((url_final.contains("https://")) == false))
						{							
									StringBuffer sb = new StringBuffer("http://");
									url_final = sb.append(url_final).toString();
									System.out.print("nikto processing...");
									System.out.println(url_final);
									nikto.Nikto1(url_final, email, response, request,session);
						}
						else
						{
							System.out.println(url_final);
							nikto.Nikto1(url_final, email, response, request,session);
						}
					}*/
					
				}
		
				int valid_count=0;
				do{
				if(session.getAttribute("validity_nmap").toString()=="0"&&session.getAttribute("validity_golismero").toString()=="0"&&session.getAttribute("validity_garmr").toString()=="0"&&session.getAttribute("validity_uniscan").toString()=="0"&&session.getAttribute("validity_spiderfoot").toString()=="0"&&session.getAttribute("validity_skipfish").toString()=="0"&&session.getAttribute("validity_ssl").toString()=="0"&&session.getAttribute("validity_zap").toString()=="0"&&session.getAttribute("validity_nikto").toString()=="0"&&session.getAttribute("validity_wapiti").toString()=="0"&&session.getAttribute("validity_wpscan").toString()=="0"){
					valid_count=1;
					
				}
			//	Thread.sleep(3600000);
				}
				while(valid_count==0);	
			}
		} 
		catch(Exception e){
			e.printStackTrace();
			System.out.println("catched");
			String error="false";
		  //	session.setAttribute("error", error);
		}

		}

		}
	

	


