package com.integration.http;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Uniscan implements Runnable {
	private static int      DISPLAY_NUMBER  = 99;
    private static String   XVFB            = "/usr/bin/Xvfb";
    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
    private static String   RESULT_FILENAME = "/tmp/screenshot.png";
    static Process p,q;
    HttpServletRequest request;
	HttpServletResponse response;
	HttpSession session;		    
	private final String email_parts;
	private final String url;
	public Uniscan(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) {
		this.request=request;
		this.response=response;			
		this.email_parts=email_parts;
		this.url=url;
		this.session=session;
	}
	public void run(){
		try{
			session.setAttribute("validity_uniscan", 1);
			Object value;
			String pid_value="";		 
			try{	
    		     	         		  	       
    		  int  i=0;                		
              Runtime rt = Runtime.getRuntime();   
              String command="perl /clouddata/qcuser/test1/uniscan6.2/uniscan.pl -u "+url+" -qweds";        
              p=rt.exec(command);             
              Class<?> clazz = p.getClass();
              if (clazz.getName().equals("java.lang.UNIXProcess")) 
              {
  					Field pidField = clazz.getDeclaredField("pid");
  					pidField.setAccessible(true);
  					value = pidField.get(p);
  					if (value instanceof Integer) {
  						System.out.println("Detected pid: " + value);
  						pid_value=value.toString();
  				}      				
  			  }
              int j=0;
              File output=new File("/clouddata/qcuser/test1/uniscan6.2/report/"+url+".html");
               do {
				if (output.exists()) 
				{
				j=1;	
				}
			  } while (j == 0);             
              String output_file="/clouddata/qcuser/test1/uniscan6.2/report/"+url+".html";            
			  String email_text = "Uniscan - Vulnerability Report";
			  System.out.println(email_text);
			  SendMail.sendmail_1(output_file, email_parts, email_text);
			  output.delete();
			  System.out.println("DESTROYED");
			  p.destroy();
			  session.setAttribute("validity_uniscan", 0);
			}
			catch(Exception e) {      	        	 	
    	 	System.out.println("catch uniscan"); 
    	 	session.setAttribute("validity_uniscan", 0);
    	 	p.destroy();

			}
}catch(Exception e){
	e.printStackTrace();
}
}
}
