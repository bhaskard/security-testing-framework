package com.integration.http;
import java.util.Timer;
import com.integration.http.ZipfileDirectory;

import com.integration.http.SendMail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.Date;
import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class Wapiti implements Runnable {
		private static int      DISPLAY_NUMBER  = 99;
	    private static String   XVFB            = "/usr/bin/Xvfb";
	    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
	    static Process p;
	    File output ;
	 //   public static void Wapiti_scan(String url, String email_parts,HttpServletResponse response, HttpServletRequest request,HttpSession session)throws IOException {
	    HttpServletRequest request;
		HttpServletResponse response;
		HttpSession session;
	    
		private final String email_parts;
		private final String url;
		public Wapiti(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) {
			this.request=request;
			this.response=response;
	
			this.email_parts=email_parts;
			this.url=url;
			this.session=session;
		}
		public void run(){
			try{
				session.setAttribute("validity_wapiti", 1);
	    Process p;
		Object value;
		double timeMilli_wapiti;
		String pid_value="";
		try {			  
			  int i = 0, j = 0;
			  Date date = new Date();    
			  long timeMilli= date.getTime();
			  double ran=Math.random();
			  timeMilli_wapiti=timeMilli*ran; 			 
			  File f = new File("/home/qcuser/results/wapiti/report_"+ timeMilli_wapiti + ".html");
			  Runtime rt1 = Runtime.getRuntime();
			 String command1="python /home/qcuser/test1/wapiti-2.3.0/bin/wapiti "+url+" -n 10 -b folder -u -v 1 -f html -o /home/qcuser/results/wapiti/"+ timeMilli_wapiti;
			  p = rt1.exec(command1);			  
			  Class<?> clazz = p.getClass();			
			  if (clazz.getName().equals("java.lang.UNIXProcess")) {
				Field pidField = clazz.getDeclaredField("pid");
				pidField.setAccessible(true);
				value = pidField.get(p);
				if (value instanceof Integer) {
					System.out.println("Detected pid: " + value);
					pid_value=value.toString();
				}
			  }	            	
			  System.out.println(command1);		
			  int flag=0;
			  File output = new File("/home/qcuser/results/wapiti/"+ timeMilli_wapiti +"/index.html");
			  String output_file = output.toString();
			  String output_folder="/home/qcuser/results/wapiti/"+ timeMilli_wapiti;
			  Thread.sleep(50000);				  
			  do{
				  RandomAccessFile stream = null;
			        try {
			            stream = new RandomAccessFile(output, "rw");
			            flag=1;
			        } catch (Exception e) {
			        	Thread.sleep(180000);
			        	flag=0;		
			            System.out.println("Skipping file " + output.getName() + " for this iteration due it's not completely written");
			        } finally {
			            if (stream != null) {
			                try {
			                    stream.close();
			                } catch (IOException e) {
			                	System.out.println("Exception during closing file " + output.getName());
			                }
			            }
			        }			        			 				  
			  }while(flag!=1);			  
			  System.out.println("OUT OF wapiti");
			  Thread.sleep(10000);
			  ZipfileDirectory.zipFolder(output_folder, output_folder+".zip");
			  String email_text = "WAPITI - Vulnerability Report";
			  System.out.println(email_text);
			  output_file=output_folder+".zip";
			  SendMail.sendmail_1(output_file, email_parts, email_text);
			  System.out.println("DESTROYED");
			 // int message=Integer.parseInt(session.getAttribute("theName").toString())+1;
		 	  //session.setAttribute("theName", message);
			  session.setAttribute("validity_wapiti", 0);
			} 
			catch (Exception e) {
				session.setAttribute("validity_wapiti", 0);
		  	System.out.println("catch wapiti");

		}
	}catch(Exception e){
		session.setAttribute("validity_wapiti", 0);
		e.printStackTrace();
	}
	
		}
}

