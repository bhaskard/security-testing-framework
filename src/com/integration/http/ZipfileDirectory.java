package com.integration.http;
import java.io.File;

import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import org.dom4j.Element;
import org.apache.commons.io.FileUtils.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.io.Files;

import sun.nio.cs.StandardCharsets;


public class ZipfileDirectory {

  public static void zipFolder(String srcFolder, String destZipFile) throws Exception {
   ZipOutputStream zip = null;
   FileOutputStream fileWriter = null;

   fileWriter = new FileOutputStream(destZipFile);
   zip = new ZipOutputStream(fileWriter);

   addFolderToZip("", srcFolder, zip);
   zip.flush();
   zip.close();
 }

  public static void addFileToZip(String path, String srcFile, ZipOutputStream zip)
     throws Exception {

   File folder = new File(srcFile);
   if (folder.isDirectory()) {
     addFolderToZip(path, srcFile, zip);
   } else {
     byte[] buf = new byte[1024];
     int len;
     FileInputStream in = new FileInputStream(srcFile);
     zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
     while ((len = in.read(buf)) > 0) {
       zip.write(buf, 0, len);
     }
   }
 }

  public static void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
     throws Exception {
   File folder = new File(srcFolder);

   for (String fileName : folder.list()) {
     if (path.equals("")) {
       addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
     } else {
       addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zip);
     }
   }
 }
}

     