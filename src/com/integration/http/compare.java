package com.integration.http;

import java.awt.Robot;

import org.openqa.selenium.chrome.ChromeDriverService;

import java.awt.event.KeyEvent;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import multiScreenShot.MultiScreenShot;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.collect.ImmutableMap;


public class compare {
	private static int      DISPLAY_NUMBER  = 99;
    private static String   XVFB            = "/usr/bin/Xvfb";
    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
    public static void compare_browsers(String testPageUrl,String email,HttpServletRequest request,HttpServletResponse response,HttpSession session){
  //  public static void  main(String args[]) {
    	try{
    		
    		Date date = new Date();    
			  long timeMilli= date.getTime();
			  double ran=Math.random();
			  double timeMilli_xdotool=timeMilli*ran; 
			  String rand=String.valueOf(timeMilli_xdotool);
	Runtime rt1 = Runtime.getRuntime();
	int i=0,j=0;	
	Process p;
	String command_create = "mkdir /clouddata/qcuser/results/xdotool/"+rand;
	  p = rt1.exec(command_create);
	  Thread.sleep(10000);
	String command = "cp /clouddata/qcuser/test1/browser-tools-master/bbshots /clouddata/qcuser/results/xdotool/"+rand+"/bbshots";
	  p = rt1.exec(command);
	  Thread.sleep(10000);
	Path path = Paths.get("/clouddata/qcuser/results/xdotool/"+rand+"/bbshots");
	Charset charset = StandardCharsets.UTF_8;
	String content = new String(Files.readAllBytes(path), charset);
	content = content.replaceAll("xdotool type 'bhaskar.png'", "xdotool type '"+rand+".png'");
	Files.write(path, content.getBytes(charset));   	
	  Thread.sleep(10000);
	  String command1 = "/clouddata/qcuser/results/xdotool/"+rand+"/bbshots -b google-chrome -u "+testPageUrl;
	  p = rt1.exec(command1);
	  File file=new File("/home/qcuser/Downloads/"+rand+".png");
	  		
	  
	  
	  /*do{
	  			if(file.exists()){
	  				i=1;
	  		     }
	  		}while(i==0);	
	  		*/
	  		Thread.sleep(120000);
	  		String command_create_fire = "mkdir /clouddata/qcuser/results/xdotool/"+rand+"/"+rand;
	  	  p = rt1.exec(command_create_fire);	
	  	String command_Chrome = "cp /clouddata/qcuser/bbshots /clouddata/qcuser/results/xdotool/"+rand+"/"+rand+"/bbshots";
	  	  p = rt1.exec(command_Chrome);
	  	  Thread.sleep(10000);
	  	String access_com = "chmod 777 /clouddata/qcuser/results/xdotool/"+rand+"/"+rand+"/bbshots";
	  	  p = rt1.exec(access_com);	  	  
	  		Path path_fire = Paths.get("/clouddata/qcuser/results/xdotool/"+rand+"/"+rand+"/bbshots");
	  		Charset charset_fir = StandardCharsets.UTF_8;
	  		String content_fir = new String(Files.readAllBytes(path_fire), charset);
	  		content_fir = content_fir.replaceAll("xdotool type 'bhaskar.png'", "xdotool type '"+rand+".png'");	  		
	  		Files.write(path_fire, content_fir.getBytes(charset_fir));   
	  	  Thread.sleep(10000);
	  		String command2 = "/clouddata/qcuser/results/xdotool/"+rand+"/"+rand+"/bbshots -b firefox -u "+testPageUrl;
	  		p = rt1.exec(command2);
	  	  Thread.sleep(10000);
	  		 File file_firefox=new File("/home/qcuser/Pictures/"+rand+".png");
	  		/*do{
	  			if(file_firefox.exists()){
	  				j=1;
	  		     }
	  		}while(j==0);
	  		*/
	  		Thread.sleep(120000);
	  		String email_text="Image Comparing";
	  		String mov_command = "cp -r /clouddata/qcuser/test1/image-comparison-slider-master/image-comparison-slider-master/ /home/qcuser/results/xdotool/"+rand+"/";
	  		p = rt1.exec(mov_command);
	  		Thread.sleep(10000);
	  		String fire_move = "mv /clouddata/qcuser/Pictures/"+rand+".png /home/qcuser/results/xdotool/"+rand+"/image-comparison-slider-master/img/img-original.png";
		  	  p = rt1.exec(fire_move);
		  	Thread.sleep(10000);
		  	String chrome_move = "mv /clouddata/qcuser/Downloads/"+rand+".png /home/qcuser/results/xdotool/"+rand+"/image-comparison-slider-master/img/img-modified.png";
		  	  p = rt1.exec(chrome_move);
		  	Thread.sleep(10000);
		  	 ZipfileDirectory.zipFolder("/home/qcuser/results/xdotool/"+rand+"/image-comparison-slider-master", "/home/qcuser/results/xdotool/"+rand+"/image-comparison-slider-master.zip");
		  	Thread.sleep(10000); 
		  	 SendMail.sendmail_1("/home/qcuser/results/xdotool/"+rand+"/image-comparison-slider-master.zip", email, email_text);
			//  SendMail.sendmail_1("/clouddata/qcuser/Downloads/"+rand+".png", email, email_text);
		  	 String xl_path="/clouddata/qcuser/apache-tomcat-8.0.18/webapps/ROOT/testing.xlsx";
			 String SheetName="Sheet3";
			 xls_read.xl_read_1(xl_path, SheetName);	
			 String Result="off";
			 xls_read.setCellData(Result, 0, 0,xl_path,SheetName);
    	
    	
    	}catch(Exception e){
    		try{
    		e.printStackTrace();
    		 String xl_path="/clouddata/qcuser/apache-tomcat-8.0.18/webapps/ROOT/";
			 String SheetName="Sheet3";
			 xls_read.xl_read_1(xl_path, SheetName);	
			 String Result="off";
			 xls_read.setCellData(Result, 0, 0,xl_path,SheetName);
    	} catch(Exception i){
    		i.printStackTrace();
    	}}
    	}
    }
    

