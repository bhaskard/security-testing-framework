package com.integration.http;
import java.util.Timer;

import com.integration.http.SendMail;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.Date;
import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class nikto implements Runnable {
		private static int      DISPLAY_NUMBER  = 99;
	    private static String   XVFB            = "/usr/bin/Xvfb";
	    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
	    static Process p;
	   // public static void Nikto1(String url, String email_parts,HttpServletResponse response, HttpServletRequest request,HttpSession session)throws IOException {
	    HttpServletRequest request;
		HttpServletResponse response;
		HttpSession session;
	    
		private final String email_parts;
		private final String url;
		public nikto(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) {
			this.request=request;
			this.response=response;
		
			this.email_parts=email_parts;
			this.url=url;
			this.session=session;
		}
		public void run(){
			try{
				 session.setAttribute("validity_nikto", 1);
	    Process p;
		Object value;
		double timeMilli_nikto;
		String pid_value="";
		try {			  
			  int i = 0, j = 0;
			  Date date = new Date();    
			  long timeMilli= date.getTime();
			  double ran=Math.random();
			  timeMilli_nikto=timeMilli*ran; 			 
			  File f = new File("/home/qcuser/results/nikto/report_"+ timeMilli_nikto + ".html");
			  Runtime rt1 = Runtime.getRuntime();
			// String command1="nmap "+url+" -oG - | perl /home/qcuser/nikto/nikto.pl -F htm -output /home/qcuser/results/nikto/report_"+ timeMilli_nikto + ".html -h -";
			  String command1 = "perl /home/qcuser/nikto-2.1.5/nikto.pl -Display E -p 80,8080,443,9880,9000,9090 -h "+ url+ " -F htm -output /home/qcuser/results/nikto/report_"+ timeMilli_nikto + ".html";
			  p = rt1.exec(command1);			  
			  Class<?> clazz = p.getClass();			
			  if (clazz.getName().equals("java.lang.UNIXProcess")) {
				Field pidField = clazz.getDeclaredField("pid");
				pidField.setAccessible(true);
				value = pidField.get(p);
				if (value instanceof Integer) {
					System.out.println("Detected pid: " + value);
					pid_value=value.toString();
				}
			  }	            	
			  System.out.println(command1);			
			  File output = new File("/home/qcuser/results/nikto/report_"+ timeMilli_nikto + ".html");
			  String output_file = output.toString();
			  Thread.sleep(50000);				  
			  do{
				  if(output.exists()==true){
					  FileInputStream fis = new FileInputStream("/home/qcuser/results/nikto/report_"+ timeMilli_nikto + ".html");
						String StringFromInputStream = IOUtils.toString(fis, "UTF-8");
						if(StringFromInputStream.contains("</html>")==true){
								System.out.println("finished ");
								j=1;
						}
						else{
							Thread.sleep(50000);
							System.out.println("not finished");
						}
				  }
			  }while(j==0);		  
			  System.out.println("OUT OF nikto");
			  Thread.sleep(10000);
			  String email_text = "NIKTO - Vulnerability Report";
			  System.out.println(email_text);
			  SendMail.sendmail_1(output_file, email_parts, email_text);
			  System.out.println("DESTROYED");
			 // int message=Integer.parseInt(session.getAttribute("theName").toString())+1;
		 	  //session.setAttribute("theName", message);
			  session.setAttribute("validity_nikto", 0);

			} 
			catch (Exception e) {
				 session.setAttribute("validity_nikto", 0);
		  	System.out.println("catch nikto");

		}
	}catch(Exception e){
		 session.setAttribute("validity_nikto", 0);
		e.printStackTrace();
	}
		}
}

