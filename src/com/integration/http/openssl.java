package com.integration.http;
import org.openqa.selenium.firefox.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import java.util.Date;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.io.File;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.TakesScreenshot;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import javax.servlet.http.*;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public class openssl {
	HttpServletRequest req;
	HttpServletResponse res;
	private static int      DISPLAY_NUMBER  = 99;
    private static String   XVFB            = "/usr/bin/Xvfb";
    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
	public static void opnssl_check(String url,String email_parts,HttpServletResponse response,HttpServletRequest request,HttpSession session) throws IOException{	
		Process p = Runtime.getRuntime().exec(XVFB_COMMAND);
        FirefoxBinary firefox = new FirefoxBinary();
        firefox.setEnvironmentProperty("DISPLAY", ":" + DISPLAY_NUMBER);
        WebDriver driver = new FirefoxDriver(firefox, null);
        //	WebDriver driver=new FirefoxDriver();
		// System.setProperty("webdriver.chrome.driver", "/home/elamathi/Downloads/chrome driver/chromedriver");
		// WebDriver driver=new ChromeDriver(); 
		
	  try{	
		  Date date = new Date();    
		  long timeMilli_open= date.getTime();
		  double ran=Math.random();
		  double time=timeMilli_open*ran; 
		  File output=new File("/home/qcuser/results/openssl/screenshot_"+time+".png");
		  System.out.print("here in ssl");
		  int i=0;		  
		  driver.get("https://www.ssllabs.com/ssltest/analyze.html?d="+url+"&ignoreMismatch=on&clearCache=on");			  
		  System.out.print("here in ssl");		
		  WebDriverWait wait_next = new WebDriverWait(driver, 400000);
	      driver.manage().timeouts().implicitlyWait( 2,TimeUnit.MINUTES);	      
	      if(driver.findElements(By.xpath("//*[@id='main']/div[1]/center/form/table/tbody/tr[1]/td[3]/input")).size() > 0==true){
	    	  File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			  FileUtils.copyFile(scrFile, new File("/home/qcuser/results/openssl/screenshot_"+time+".png"));
			  System.out.print("IN IF");			
	      }
	      else{
	    	  	System.out.print("1");
	    	  	wait_next.until(ExpectedConditions.visibilityOfElementLocated(By.className("reportTime"))).isDisplayed();	    	
	    	  	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File("/home/qcuser/results/openssl/screenshot_"+time+".png"));
				System.out.print("IN ELSE");
			      }
		  do{
            if(output.exists()==true){
          	  System.out.print("created");
          	  i=1;
            }                 
             Thread.sleep(5000);   
             System.out.println("not finished ssl"+url);
            
            }while(i==0);
		  	  String output_file=output.toString();
	  	   	  String email_text="OPEN SSL - Vulnerability Report";
	  	   	  SendMail.sendmail_1(output_file,email_parts,email_text);	  	   	  
	  	   	  driver.quit();
	  	   	  p.destroy();

		}
	  	catch(Exception e){
	  		driver.quit();
	  		String error="false";
	  		System.out.print("exception caught in ssl");
		  	 p.destroy();
		  /*	session.setAttribute("error", error);
	  		int message=Integer.parseInt(session.getAttribute("theName").toString())+1;
		  	session.setAttribute("theName", message);
	  		e.printStackTrace();  		
	  		String tool="openssl";
	  		if(session.getAttribute("tools_exception").toString().equalsIgnoreCase("")!=true)
	  		{
	  			String exception=tool+","+session.getAttribute("tools_exception").toString();
	  			session.setAttribute("tools_exception",exception);
	  		}
	  		else{
	  			
	  			session.setAttribute("tools_exception",tool);
	  		}*/ 
	  }
	}

}
