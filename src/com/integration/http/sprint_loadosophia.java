package com.integration.http;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

public class sprint_loadosophia {
	private static int      DISPLAY_NUMBER  = 99;
    private static String   XVFB            = "/usr/bin/Xvfb";
    private static String   XVFB_COMMAND    = XVFB + " :" + DISPLAY_NUMBER;
	public static void loadosophia(String file,String comp,String email_parts) throws IOException{	
	//	public static void main(String args[]) throws IOException{
		//	String file="/home/bhaskar/Downloads/testing16.jtl";
		//	String comp="testing16.jtl";
		Process p = Runtime.getRuntime().exec(XVFB_COMMAND);
			FirefoxBinary firefox = new FirefoxBinary();
			firefox.setEnvironmentProperty("DISPLAY", ":" + DISPLAY_NUMBER);  
			System.out.println("starting browser");
			ProfilesIni profile = new ProfilesIni();
			String download_path = "/home/qcuser/Downloads";
			FirefoxProfile myprofile = profile.getProfile("load_testing");
			myprofile.setPreference("browser.download.folderList", 2);
			myprofile.setPreference("browser.download.dir", download_path);
			myprofile.setPreference("browser.download.manager.showWhenStarting",false);
			myprofile.setPreference("browser.download.alertOnEXEOpen", false);
			myprofile.setPreference("browser.helperApps.neverAsksaveToDisk", "application/pdf,application/x-pdf");
			myprofile.setPreference("browser.download.manager.focusWhenStarting", false);
			myprofile.setPreference("browser.helperApps.alwaysAsk.force", false);
			myprofile.setPreference("browser.download.manager.alertOnEXEOpen", false);
			myprofile.setPreference("browser.download.manager.closeWhenDone", false);
			myprofile.setPreference("browser.download.manager.showAlertOnComplete", false);
			myprofile.setPreference("browser.download.manager.useWindow", false);
			myprofile.setPreference("browser.download.manager.showWhenStarting", false);
			myprofile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);		
			WebDriver driver = new FirefoxDriver(firefox, myprofile);	
			System.out.println("started browser");
        try{
        	System.out.println("1");
        	driver.get("https://loadosophia.org/login/");
        	System.out.println("1");
        	/*driver.findElement(By.linkText("Sign in with Google")).click();
        	System.out.println("1");
        	driver.findElement(By.id("Email")).sendKeys("xedmtester");
        	System.out.println("1");
        	driver.findElement(By.id("Passwd")).sendKeys("xerago666");
        	System.out.println("1");
        	driver.findElement(By.id("signIn")).click();
        	System.out.println("1");
         //	driver.findElement(By.xpath("//*[@id='submit_approve_access']")).click();
         //	System.out.println("1");*/
        	driver.findElement(By.xpath("html/body/div[1]/nav/ul[1]/li[1]/a")).click();
        	System.out.println("1");
        	WebElement fileInput = driver.findElement(By.name("jtl_file"));
        	System.out.println("1a");
        	fileInput.sendKeys(file);
        	System.out.println("1");
        	driver.findElement(By.xpath("html/body/div[2]/div[2]/div[2]/div/div[2]/div[1]/div/div[2]/div/form/div[3]/div/input[1]")).click();
        	WebElement table_element = driver.findElement(By.xpath("//*[@id='uploadsPage']/div/div"));
               List<WebElement> tr_collection=table_element.findElements(By.className("file-row"));
               System.out.println("NUMBER OF ROWS IN THIS TABLE = "+tr_collection.size());
               int row_num,col_num;
               row_num=1;
               String chang="";
               for(WebElement trElement : tr_collection)
               {
                   List<WebElement> td_collection=trElement.findElements(By.xpath("td"));
                   System.out.println("NUMBER OF COLUMNS="+td_collection.size());
                   col_num=1;
                   for(WebElement tdElement : td_collection)
                   {
                    System.out.println("row # "+row_num+", col # "+col_num+ "text="+tdElement.getText());
                    if(tdElement.getText().equalsIgnoreCase(comp)){
                    	chang=tdElement.getAttribute("id").toString();
                    	chang=chang.replace("filename", "");
                    }
                    col_num++;
                   }
                   row_num++;
                }
               int st=0;
               String xpat="//*[@id='status"+chang+"']/span/a";
               Thread.sleep(100000);
               do{
               if(driver.findElement(By.xpath(xpat)).getText().equalsIgnoreCase("Report Ready")){
            	   st=1;
               }
               
        }while(st==0);
               String url="";
               if(st==1){
            	   driver.findElement(By.xpath(xpat)).click();
            	   url=driver.findElement(By.xpath("html/body/div[2]/div[2]/div[1]/div[1]/a[3]")).getText();
            	   driver.findElement(By.xpath("//*[@id='reportTabs']/div/div/div[3]/a[1]")).click();
               }
               int in=0;
               File output=new File("/clouddata/qcuser/Downloads/loadosophia-report-"+url+".pdf");
               Thread.sleep(200000);
				String output_file=output.toString();
	            do{
	            	if(output.exists()==true){
	            		System.out.print("created");
	            		in=1;
	            	}     
	            	else
	            	{
	            		Thread.sleep(200000);
	            		System.out.println("not finished load testing"+url);
	            	}
	            }while(in==0);
	            String email_text="Load testing Report";
	         //   String email_parts="bhaskar.fame@gmail.com";
	            SendMail.sendmail_1(output_file,email_parts,email_text);  
	            p.destroy();
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        	System.out.println("exception caught in sprint loadosophia");
        	p.destroy();
        }
	}
}
