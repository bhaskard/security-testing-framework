

package com.integration.http;

import org.apache.poi.xssf.usermodel.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class xls_read {
//	public static String xlspath="https://www.ipb.citibank.com.sg/english/forms/english/open-an-IPB-account/form.htm";
	public FileInputStream fis= null;
	public static FileOutputStream fos=null;
	private static XSSFWorkbook workbook;
	private static XSSFSheet sheet=null;
	private static XSSFRow row=null;
	private static XSSFCell cell= null;
	public static String CellData;
	
	public static void xl_read_1(String path,String sheetName) throws Exception
	{
		
		try{
			FileInputStream fis=new FileInputStream(path);
			workbook= new XSSFWorkbook(fis);
			sheet = workbook.getSheet(sheetName);
			fis.close();
		}
		catch(Exception e){
			throw(e);
		}
		}
	
	public static String getCellData(String sheetName,int RowNum, int ColNum){
		try{	
		 cell=sheet.getRow(RowNum).getCell(ColNum);
		 String CellData= cell.getStringCellValue();
		 return CellData;
	}
		catch(Exception e){
			return "";
		}
		
	}
	@SuppressWarnings("static-access")
	public static void setCellData(String Result,int RowNum, int ColNum,String path,String sheetName) throws Exception
	{
		try{
			FileInputStream fis=new FileInputStream(path);
			workbook= new XSSFWorkbook(fis);
			sheet = workbook.getSheet(sheetName);
			row=sheet.getRow(RowNum);
			cell=row.getCell(ColNum);
			if(cell==null)
			{
				cell = row.createCell(ColNum);
				cell=row.getCell(ColNum);
				cell.setCellValue(Result);
				System.out.println("data written");
				fos = new FileOutputStream(path);

	            workbook.write(fos);
                
	            fos.close();
				
			}
			else{
				cell.setCellValue(Result);
				System.out.println("data written");
				fos = new FileOutputStream(path);

	            workbook.write(fos);
                
	            fos.close();
			}
		}
			catch(Exception e)
			{
				throw(e);
			}
		}
	
			
		
		
	
	
	
	
	
	
		
	}
	
	
	